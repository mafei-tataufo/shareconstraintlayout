package me.bakumon.shareconstraintlayout

import android.content.Context
import android.util.AttributeSet
import android.view.ViewAnimationUtils
import androidx.constraintlayout.widget.ConstraintHelper
import androidx.constraintlayout.widget.ConstraintLayout
import kotlin.math.hypot

class CircularRevealHelper(context: Context, attributeSet: AttributeSet) : ConstraintHelper(context, attributeSet) {

    override fun updatePostLayout(container: ConstraintLayout) {
        super.updatePostLayout(container)

        referencedIds.forEach {
            val view = container.getViewById(it)
            val radius = hypot(view.width.toDouble(), view.height.toDouble()).toFloat()
            ViewAnimationUtils.createCircularReveal(view, 0, 0, 0f, radius)
                    .setDuration(2000)
                    .start()
        }

    }
}