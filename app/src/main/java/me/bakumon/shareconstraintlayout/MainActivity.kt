package me.bakumon.shareconstraintlayout

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onClickGroup(view: View) {
        startActivity(Intent(this, GroupActivity::class.java))
    }

    fun onClickLayer(view: View) {
        startActivity(Intent(this, LayerActivity::class.java))
    }

    fun onClickPlaceholder(view: View) {
        startActivity(Intent(this, PlaceholderActivity::class.java))
    }

    fun onClickCircularReveal(view: View) {
        startActivity(Intent(this, CircularRevealActivity::class.java))
    }

    fun onClickUpdateConstraint(view: View) {
        startActivity(Intent(this, UpdateConstraintActivity::class.java))
    }

    fun onClickUpdateConstraintAuto(view: View) {
        startActivity(Intent(this, UpdateConstraintStartEndActivity::class.java))
    }
}