package me.bakumon.shareconstraintlayout

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class CircularRevealActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_circular_reveal)
    }
}