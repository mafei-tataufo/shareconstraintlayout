package me.bakumon.shareconstraintlayout

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.helper.widget.Layer
import androidx.core.view.isVisible

class LayerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_layer)
    }

    fun onClick(view: View) {
        // 也可以设置可见性
        val layer = findViewById<Layer>(R.id.layer)
        layer.rotation = 45f
        layer.translationX = 100f
        layer.translationY = 100f
    }
}